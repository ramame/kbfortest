<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Status;
use app\models\Level;

/* @var $this yii\web\View */
/* @var $model app\models\Breakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="breakdown-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(
        ArrayHelper::map(Status::find()->asArray()->all(), 'id', 'status_name')) ?>

    <?= $form->field($model, 'level')->dropDownList(
        ArrayHelper::map(Level::find()->asArray()->all(), 'id', 'level_name')) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
