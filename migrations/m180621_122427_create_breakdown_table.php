<?php

use yii\db\Migration;

/**
 * Handles the creation of table `breakdown`.
 */
class m180621_122427_create_breakdown_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->createTable('breakdown', [
           'id' => $this->primaryKey(),
           'title'=> $this->string(),
           'status' => $this->integer(),
           'level' => $this->integer(),
          
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('breakdown');
    }
}
