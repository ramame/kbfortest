<?php

use yii\db\Migration;

/**
 * Class m180621_133647_init_rbac
 */
class m180621_133647_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $auth = Yii::$app->authManager;

      $admin = $auth->createRole('Admin');                //הרשאות
      $auth->add($admin);

      $teamleader = $auth->createRole('Teamleader');
      $auth->add($teamleader);

      $member = $auth->createRole('Member');
      $auth->add($member);

      $manager = $auth->createRole('Manager');
      $auth->add($manager);
              
      $auth->addChild($admin, $teamleader);
      $auth->addChild($teamleader, $member);
      $auth->addChild($member, $manager);

      $manageUsers = $auth->createPermission('manageUsers');                    //פעולות
      $auth->add($manageUsers);

       $updateLevel= $auth->createPermission('updateLevel');
      $auth->add($updateLevel);    

      $manageBreakdown = $auth->createPermission('manageBreakdown');
      $auth->add($manageBreakdown);        

      $managerview = $auth->createPermission('managerview');
      $auth->add($managerview);                 
              
      $viewOwnDeatils = $auth->createPermission('viewOwnDeatils');  //rule 

      $rule = new \app\rbac\MemberRule;
      $auth->add($rule);
              
      $viewOwnDeatils->ruleName = $rule->name;                
      $auth->add($viewOwnDeatils);                 
                                
              
      $auth->addChild($admin, $manageUsers);
      $auth->addChild($teamleader, $updateLevel); 
      $auth->addChild($member, $manageBreakdown);
      $auth->addChild($manager, $managerview);
      $auth->addChild($viewOwnDeatils, $manageBreakdown);

        $auth->assign($member, 2);
        $auth->assign($admin, 1);
        $auth->assign($teamleader, 3);
        $auth->assign($manager, 4);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_133647_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_133647_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
