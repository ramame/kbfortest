<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "breakdown".
 *
 * @property int $id
 * @property string $title
 * @property int $status
 * @property int $level
 */
class Breakdown extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'breakdown';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'level'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
            'level' => 'Level',
        ];
    }


}
